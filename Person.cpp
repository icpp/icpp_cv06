#include "person.h"
using namespace std;

ostream& operator<<(ostream& output, const Address& address) {
    output << address._street << "\n" << address._city << "\n" << address._postalCode << endl;
    return output;
}

istream& operator>>(istream& input, Address& address) {
    input >> address._street;
    input >> address._city;
    input >> address._postalCode;
    return input;
}

ostream& operator<<(ostream& output, const Date& date) {
    output << date._day << "\n" << date._month << "\n" << date._year << endl;
    return output;
}

istream& operator>>(istream& input, Date& date) {
    input >> date._day;
    input >> date._month;
    input >> date._year;
    return input;
}

ostream& operator<<(ostream& output, const Person& aPerson) {
    output << aPerson._name << "\n" << aPerson._lastname << "\n" << aPerson._address << "\n" << aPerson._date << "\n" << endl;
    return output;
}

istream& operator>>(istream& input, Person& aPerson) {
    input >> aPerson._name;
    input >> aPerson._lastname;
    input >> aPerson._address;
    input >> aPerson._date;
    return input;
}

void saveBinary(std::ofstream& output, const Person& aPerson)
{
    int size = aPerson._name.size() +1;
    output.write((const char*)&size, sizeof(int));
    output.write(reinterpret_cast<char*>(&size), sizeof(int));
    output.write(aPerson._name.c_str(), size);

    size = aPerson._lastname.size() + 1;
    output.write((const char*)&size, sizeof(int));
    output.write(reinterpret_cast<char*>(&size), sizeof(int));
    output.write(aPerson._lastname.c_str(), size);

    output << aPerson._address;
    output << aPerson._date;
}

void loadBinary(std::ifstream& input, Person& aPerson)
{
    char* buf;
    int size = 0;

    input.read((char*) & (size), sizeof(int));
    input.read(reinterpret_cast<char*>(&size), sizeof(int));
    buf = new char[size];
    input.read(buf, size);
    aPerson._name.append(buf, size);

    input.read((char*) & (size), sizeof(int));
    input.read(reinterpret_cast<char*>(&size), sizeof(int));
    buf = new char[size];
    input.read(buf, size);
    aPerson._lastname.append(buf, size);

    input >> aPerson._address;
    input >> aPerson._date;

}

void saveBinary(std::ofstream& output, const Address& aAddress)
{
    int size = aAddress._city.size() +1;

    output.write((const char*)&size, sizeof(int));
    output.write(reinterpret_cast<char*>(&size), sizeof(int));
    output.write(aAddress._city.c_str(), size);


    size = aAddress._street.size() +1;
    output.write((const char*)&size, sizeof(int));
    output.write(reinterpret_cast<char*>(&size), sizeof(int));
    output.write(aAddress._street.c_str(), size);

    output.write((const char*)&aAddress._postalCode, sizeof(int));
}

void loadBinary(std::ifstream& input, Address& aAddress)
{
    char* buf;
    int size = 0;

    input.read((char*) & (size), sizeof(int));
    input.read(reinterpret_cast<char*>(&size), sizeof(int));
    buf = new char[size];
    input.read(buf, size);
    aAddress._city.append(buf, size);

    input.read((char*)&(size), sizeof(int));
    input.read(reinterpret_cast<char*>(&size), sizeof(int));
    buf = new char[size];
    input.read(buf, size);
    aAddress._city.append(buf, size);

    input.read((char*) & (aAddress._postalCode), sizeof(int));
}

void saveBinary(std::ofstream& output, const Date& aDate)
{
    output.write((const char *)&aDate._day, sizeof(int));
    output.write((const char*)&aDate._month, sizeof(int));
    output.write((const char*)&aDate._year, sizeof(int));
}

void loadBinary(std::ifstream& input, Date& aDate)
{
    input.read((char *)&(aDate._day), sizeof(int));
    input.read((char*) & (aDate._month), sizeof(int));
    input.read((char*) & (aDate._year), sizeof(int));
}
