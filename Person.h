#ifndef PERSON_H
#define PERSON_H

#include <string>
#include <iostream>
#include <fstream>

using namespace std;

struct Date {
    int _day;
    int _month;
    int _year;
};

ostream& operator<<(ostream& output, const Date& aDate);
istream& operator>>(istream& input, Date& aDate);

void saveBinary(ofstream& output, const Date& aDate);
void loadBinary(ifstream& input, Date& aDate);

struct Address {
    string _street;
    string _city;
    int _postalCode;
};

ostream& operator<<(ostream& output, const Address& aAddress);
istream& operator>>(istream& input, Address& aAddress);

void saveBinary(ofstream& output, const Address& aAddress);
void loadBinary(ifstream& input, Address& aAddress);

struct Person {
    string _name;
    string _lastname;
    Address _address;
    Date _date;
};

ostream& operator<<(ostream& output, const Person& aPerson);
istream& operator>>(istream& input, Person& aPerson);

void saveBinary(ofstream& output, const Person& aPerson);
void loadBinary(ifstream& input, Person& aPerson);

#endif
