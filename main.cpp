#include <iostream>
#include <fstream>
#include "Person.h"

#define MAXSIZE 5

using namespace std;

Person generatePerson(int i);
void save();
void load();
void saveBin();
void loadBin();

int main() {
    save();
    load();
    saveBin();
    loadBin();
    return 0;
}

Person generatePerson(int aI) {
    Address address = {"Street ", "City", 12345 + aI};
    Date date = {aI, aI, 1000 + aI};
    Person person = {"User", "UserLastName", address, date};
    return person;
}


void save() {
    Person persons[MAXSIZE];
    for (int i = 0; i < MAXSIZE; ++i) {
        persons[i] = generatePerson(i);
    }

    ofstream file;
    file.open("file.txt");

    if (file.is_open()) {
        file << MAXSIZE << std::endl;
        for (int i = 0; i < MAXSIZE; ++i) {
            file << persons[i];
        }
        file.close();
        cout << "The file was saved." << endl;
    } else {
        cout << "The file was not saved." << endl;

    }
}

void saveBin() {
    Person persons[MAXSIZE];
    for (int i = 0; i < MAXSIZE; ++i) {
        persons[i] = generatePerson(i);
    }

    ofstream file;
    file.open("file.bin");
    int max = MAXSIZE;
    file.write((const char*)&max, sizeof(int));

    if (file.is_open()) {
        for (int i = 0; i < MAXSIZE; ++i) {
            saveBinary(file, persons[i]);
        }
        file.close();
        cout << "The binary file was saved." << endl;
    } else {
        cout << "The binary file was not saved." << endl;

    }
}

void load() {
    int size;
    ifstream file{};
    file.open("file.txt");
    file >> size;

    Person *persons = new Person[size];

    for (int i = 0; i < MAXSIZE; i++) {
        file >> persons[i];
    }

    delete[] persons;
    cout << "The file was loaded." << endl;
}

void loadBin() {
    int max = 0;
    ifstream file{};

    file.open("file.bin");

    file.read((char*)&max, sizeof(int));
    Person* persons = new Person[max];

    for (int i = 0; i < max; i++) {
        loadBinary(file, persons[i]);
    }

    delete[] persons;
}









